# Hifi Audio

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## External requirements

* [MPD](https://www.musicpd.org/)
* [mpc](https://github.com/MusicPlayerDaemon/mpc) - command-line client for MPD
* [GMusicProxy](https://bitbucket.org/helen_lawes/gmusicproxy) - customised fork of this [original](https://github.com/diraimondo/gmusicproxy)

### MPD configuration

Configure MPD 'music_folder' to point to:

**{website_root}**/app/playlists

as it cannot seem to find playlists outside of that.

MPD and GMusicProxy should be setup to run as services


## Development server

Run `ng serve` for a dev server and `npm run server` for backend API. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Websocket API

This API sends and receives data in the JSON format and is passed to/from the front-end using websockets. The websocket transfer of data is handled by the node module `socket.io`.

## LISTEN TO `play`

Upon receiving a `play` message, the server will load the specified/default playlist into the MPD, if no playlist is currently loaded and then will send `mpd play` to the system, to start playing the songs.

### Sending properties

name | description | type
--- | --- | ---
playing | whether track is to be played | boolean
firstPlay | whether need to load file into queue | boolean
file | name of cached playlist file | string/null

**Example client sending:**

```
socket.emit('play',{
    playing: true,
    firstPlay: true,
    file: 'album-Bcj5q...wwv.m3u'
});
```

### Response properties

If the request was successful, the server will send back the same data that was sent from the client as the value of the message property. If an error occurs then an error response will be emitted.

**Example server successful response:**

```
socket.emit('message', {
    name: 'play',
    message: {
        playing: true,
        firstPlay: true,
        file: 'album-Bcj5q...wwv.m3u'
    }
});
```

**Example server error response:**

```
socket.emit('app-error', {
    name: 'play',
    message: 'Could not play song'
});
```
