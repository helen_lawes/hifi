import { Injectable } from '@angular/core';

export class ObjectId {

	increment = Math.floor(Math.random() * (16777216));
	pid = Math.floor(Math.random() * (65536));
	machine = Math.floor(Math.random() * (16777216));
	timestamp;

	constructor(...args) {
		this.checkStorage();

		if (typeof (args[0]) === 'object') {
			Object.assign(this, args[0]);
		} else if (typeof (args[0]) === 'string' && args[0].length === 24) {
			this.timestamp = Number('0x' + args[0].substr(0, 8)),
			this.machine = Number('0x' + args[0].substr(8, 6)),
			this.pid = Number('0x' + args[0].substr(14, 4)),
			this.increment = Number('0x' + args[0].substr(18, 6));
		} else if (args.length === 4 && args[0] != null) {
			let [ timestamp, machine, pid, increment ] = args;
			Object.assign(this, { timestamp, machine, pid, increment });
		} else {
			this.timestamp = Math.floor(new Date().valueOf() / 1000);
			this.increment++;
			if (this.increment > 0xffffff) {
				this.increment = 0;
			}
		}
	}

	setMachineCookie() {
		const cookieList = document.cookie.split('; ');
		for (let i in cookieList) {
			let cookie = cookieList[i].split('=');
			let cookieMachineId = parseInt(cookie[1], 10);
			if (cookie[0] === 'mongoMachineId' && cookieMachineId && cookieMachineId >= 0 && cookieMachineId <= 16777215) {
				this.machine = cookieMachineId;
				break;
			}
		}
		document.cookie = 'mongoMachineId=' + this.machine + ';expires=Tue, 19 Jan 2038 05:00:00 GMT;path=/';
	}

	checkStorage() {

		if (typeof (localStorage) !== 'undefined') {
			try {
				let mongoMachineId = parseInt(localStorage['mongoMachineId'], 10);
				if (mongoMachineId >= 0 && mongoMachineId <= 16777215) {
					this.machine = Math.floor(localStorage['mongoMachineId']);
				}
				// Just always stick the value in.
				localStorage['mongoMachineId'] = this.machine;
			} catch (e) {
				this.setMachineCookie();
			}
		} else {
			this.setMachineCookie();
		}
	}

	getDate() {
		return new Date(this.timestamp * 1000);
	}

	toArray() {
		let strOid = this.toString();
		let array = [];
		let i;
		for (i = 0; i < 12; i++) {
			array[i] = parseInt(strOid.slice(i * 2, i * 2 + 2), 16);
		}
		return array;
	}

	toString() {
		if (this.timestamp === undefined
			|| this.machine === undefined
			|| this.pid === undefined
			|| this.increment === undefined) {
			return 'Invalid ObjectId';
		}

		let timestamp = this.timestamp.toString(16);
		let machine = this.machine.toString(16);
		let pid = this.pid.toString(16);
		let increment = this.increment.toString(16);
		return '00000000'.substr(0, 8 - timestamp.length) + timestamp +
			'000000'.substr(0, 6 - machine.length) + machine +
			'0000'.substr(0, 4 - pid.length) + pid +
			'000000'.substr(0, 6 - increment.length) + increment;
	}
}
