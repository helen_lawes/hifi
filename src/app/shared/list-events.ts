import { ActivatedRoute } from '@angular/router';
import { MusicService } from 'core/music.service';

export class ListEvents {

	playing = false;
	error = false;
	firstPlay: any = true;
	playlistFile = null;
	playFrom = true;
	songs = [];
	loading = false;
	playListImage = '';

	constructor(protected _ms: MusicService) {}

}
