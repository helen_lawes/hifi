import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MusicService } from 'core/music.service';
import { ListEvents } from 'shared/list-events';

@Component({
	selector: 'app-queue',
	templateUrl: './queue.component.html',
	styleUrls: ['./queue.component.scss']
})
export class QueueComponent extends ListEvents implements OnInit {

	constructor(protected _route: ActivatedRoute, protected _ms: MusicService) {
		super(_ms);
		this.playlistFile = `queue.m3u`;
		this._ms.subscribe((event) => {
			if (event.type === 'songs') {
				this.songs = event.message;
				this.loading = false;
				this.error = false;
			} else if (event.type === 'queue-error') {
				this.loading = false;
				this.error = true;
			}
		});
		this.getQueue();
	}

	ngOnInit() {
	}

	getQueue() {
		this.loading = true;
		this.error = false;
		this._ms.getSongQueue();
	}

}
