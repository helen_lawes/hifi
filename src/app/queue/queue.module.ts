import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'core/core.module';
import { QueueComponent } from './queue.component';

import { QueueRoutingModule } from './queue-routing.module';


@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		QueueRoutingModule
	],
	declarations: [QueueComponent]
})
export class QueueModule { }
