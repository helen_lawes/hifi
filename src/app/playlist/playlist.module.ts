import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'core/core.module';
import { PlaylistComponent } from './playlist.component';

import { PlaylistRoutingModule } from './playlist-routing.module';

@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		PlaylistRoutingModule
	],
	declarations: [PlaylistComponent]
})
export class PlaylistModule { }
