import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MusicService } from 'core/music.service';
import { ListEvents } from 'shared/list-events';

@Component({
	selector: 'app-playlist',
	templateUrl: './playlist.component.html',
	styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent extends ListEvents implements OnInit, OnDestroy {

	playlistId = null;
	playlistName = null;
	type = null;
	route = null;
	secondaryHeading = null;
	subscriptions = [];

	constructor(protected _route: ActivatedRoute, protected _ms: MusicService) {
		super(_ms);
		this.firstPlay = true;
		this.subscriptions.push(this._route.url.subscribe(url => {
			this.route = url[0].path;
		}));
		this.subscriptions.push(this._route.params.subscribe(params => {
			this.playlistId = params['playlistId'];
			this.playlistName = this.unescape(params['playListName']);
			this.type = params['type'];
			this.getPlaylist();
			if (this.route === 'im-feeling-lucky') {
				this.playlistFile = 'ifl.m3u';
			} else if (this.type) {
				this.playlistFile = `${this.route}-${this.type}-${this.playlistId}.m3u`;
			} else {
				this.playlistFile = `${this.route}-${this.playlistId}.m3u`;
			}
		}));
		this.subscriptions.push(this._ms.subscribe((event) => {
			if (event.type === 'songs' && event.id === this.playlistId) {
				this.songs = event.message;
				this.loading = false;
				this.error = false;
				this.playListImage = this.songs[0].image;
				this.secondaryHeading = this.route === 'album' ? this.songs[0].artist : null;
			} else if (event.type === 'playlist-error' && event.id === this.playlistId) {
				this.loading = false;
				this.error = true;
			}
		}));
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sub => {
			sub.unsubscribe();
		});
	}

	unescape(string = '') {
		return string.replace('%28', '(').replace('%29', ')');
	}

	getPlaylist(refresh = false) {
		this.loading = true;
		this.error = false;
		this._ms.getPlaylist(this.playlistId, this.route, this.type, refresh);
	}

	refresh() {
		this.getPlaylist(true);
	}

}
