import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaylistComponent } from './playlist.component';

const routes: Routes = [
	{ path: 'playlist/:playlistId/:playListName', component: PlaylistComponent },
	{ path: 'album/:playlistId/:playListName', component: PlaylistComponent },
	{ path: 'station/:type/:playlistId/:playListName', component: PlaylistComponent },
	{ path: 'station/:playlistId/:playListName', component: PlaylistComponent },
	{ path: 'im-feeling-lucky', component: PlaylistComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PlaylistRoutingModule { }
