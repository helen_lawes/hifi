import { Component, ViewChild } from '@angular/core';
import { MusicService } from 'core/music.service';
import { SnackbarComponent } from 'core/snackbar/snackbar.component';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	errorMessage;
	snackOpen: any = false;
	hasDisconnected = false;
	@ViewChild('errorSnack') errorSnack: SnackbarComponent;
	@ViewChild('offlineSnack') offlineSnack: SnackbarComponent;
	@ViewChild('onlineSnack') onlineSnack: SnackbarComponent;

	constructor(private _ms: MusicService) {
		this._ms.subscribe((event) => {
			if (event.type === 'error') {
				this._closeOpenSnack();
				this.errorMessage = event.message;
				this.errorSnack.show();
				this.snackOpen = 'errorSnack';
			} else if (event.type === 'disconnected') {
				this._closeOpenSnack();
				this.offlineSnack.show();
				this.snackOpen = 'offlineSnack';
				this.hasDisconnected = true;
			} else if (event.type === 'connected') {
				this._closeOpenSnack();
				if (this.hasDisconnected) {
					this.onlineSnack.show(5000);
					this.snackOpen = 'onlineSnack';
				}
			}
		});
	}

	_closeOpenSnack() {
		if (!this.snackOpen) { return; }
		this[this.snackOpen].hide();
	}
}
