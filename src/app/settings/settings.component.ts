import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MusicService } from 'core/music.service';
import { MenuService } from 'core/menu.service';
import { ObjectId } from 'shared/utilities';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

	subscriptions = [];
	cacheTotal = 0;
	cacheSize = 0;
	cacheExpiry = 0;
	speakers = [];
	currentSpeaker = null;
	settingsSection = null;
	settingsId = null;

	constructor(private _route: ActivatedRoute, private _router: Router, private _ms: MusicService, private _menu: MenuService) {
		this.subscriptions.push(this._route.params.subscribe(params => {
			if ( 'speakerId' in params ) {
				this.settingsId = params.speakerId;
				this.settingsSection = 'speaker';
			} else {
				this.settingsId = null;
				this.settingsSection = null;
			}
		}));
		this.subscriptions.push(this._ms.subscribe((event) => {
			if (event.type === 'cache') {
				this.cacheTotal = event.message.total;
				this.cacheSize = event.message.size;
				this.cacheExpiry = event.message.expiry;
			} else if (event.type === 'speakers') {
				this.speakers = event.message;
				if (this.settingsId) {
					this.currentSpeaker = this.speakerById(this.settingsId);
					this._menu.formPage(this.currentSpeaker.name || 'New speaker');
				}
			}
		}));
		this.subscriptions.push(this._menu.subscribe((event) => {
			if (event.type === 'formSave') {
				if (this.settingsSection === 'speaker') {
					this._ms.updateSpeaker(this.currentSpeaker);
					this.rootSettings();
				}
			}
		}));
		this._ms.getSettings();
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sub => {
			sub.unsubscribe();
		});
	}

	rootSettings() {
		this._router.navigate(['/settings']);
	}

	openSettings(section, id) {
		this._router.navigate(['/settings', section, id]);
	}

	speakerById(id) {
		if ( id === 'new' ) {
			return {
				id: new ObjectId().toString(),
				name: null,
				ip: null,
				active: false
			};
		}
		return this.speakers.filter(speaker => speaker.id === id)[0];
	}

	deleteSpeaker(id) {
		this._ms.deleteSpeaker(id);
		this.rootSettings();
	}

}
