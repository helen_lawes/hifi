import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'core/core.module';
import { SettingsComponent } from './settings.component';

import { SettingsRoutingModule } from './settings-routing.module';
import { SectionComponent } from './section/section.component';
import { ItemComponent } from './item/item.component';

@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		SettingsRoutingModule
	],
	declarations: [SettingsComponent, SectionComponent, ItemComponent]
})
export class SettingsModule { }
