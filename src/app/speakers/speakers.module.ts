import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'core/core.module';
import { SpeakersComponent } from './speakers.component';

import { SpeakersRoutingModule } from './speakers-routing.module';

@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		SpeakersRoutingModule
	],
	declarations: [SpeakersComponent]
})
export class SpeakersModule { }
