import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MusicService } from 'core/music.service';

@Component({
	selector: 'app-speakers',
	templateUrl: './speakers.component.html',
	styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit, OnDestroy {

	subscriptions = [];
	speakers = [];
	loaded = false;

	constructor(private _route: ActivatedRoute, private _router: Router, private _ms: MusicService) {
		this.subscriptions.push(this._ms.subscribe((event) => {
			if (this.loaded) return;
			if (event.type === 'speakers') {
				this.speakers = event.message;
				this.loaded = true;
			}
		}));
		this._ms.getSpeakers();
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sub => {
			sub.unsubscribe();
		});
	}

	speakerToggle(device) {
		this._ms.toggleSpeaker(device.id, device.active);
	}

}
