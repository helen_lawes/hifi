import { Component, OnInit } from '@angular/core';
import { MusicService } from 'core/music.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	songs = [];
	collections = [];
	search = '';
	loading = true;
	playing = false;
	error = false;

	constructor(private _ms: MusicService) {
		this._ms.subscribe((event) => {
			if (event.type === 'collections') {
				this.collections = event.message;
				this.loading = false;
				this.error = false;
			} else if (event.type === 'collections-error') {
				this.loading = false;
				this.error = true;
			}
		});
		this.getCollections();
	}

	ngOnInit() {
	}

	getCollections() {
		this.loading = true;
		this.error = false;
		this._ms.getCollections();
	}

}
