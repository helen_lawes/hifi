import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'core/core.module';
import { HomeComponent } from './home.component';

import { HomeRoutingModule } from './home-routing.module';

@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		HomeRoutingModule
	],
	declarations: [HomeComponent]
})
export class HomeModule { }
