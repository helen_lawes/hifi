import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'join'})
export class JoinPipe implements PipeTransform {
	transform(v: any, args: any[]) {
		if (typeof v.join !== 'function') { return v; }
		return v.join(args ? args[0] : ', ');
	}
}

@Pipe({name: 'hideEmpty'})
export class HideEmptyPipe implements PipeTransform {
	transform(v: any, args: any[]) {
		if (typeof v === 'undefined' || !v) { return ''; }
		return v;
	}
}

@Pipe({name: 'truncate'})
export class TruncatePipe implements PipeTransform {
	transform(v: any, length: number) {
		if (typeof v === 'undefined' || !v) { return ''; }
		if (typeof v === 'string') {
			const parts = v.split(' ');
			let string = '';
			let temp = '';
			for (let i = 0; i < parts.length; i++) {
				temp += ' ' + parts[i];
				if (temp.length < length) {
					string = temp;
				} else {
					return string.substr(1) + '...';
				}
			}
			return string.substr(1);
		}
		return v;
	}
}

@Pipe({name: 'humanTime'})
export class HumanTimePipe implements PipeTransform {
	transform(input: any, args: any[]) {
		let seconds = parseInt(input, 10);
		if (!isNaN(seconds)) {

			let mins = Math.floor(seconds / 60);
			if (mins > 0) {
				seconds = (seconds - (mins * 60));
			}
			mins = mins || 0;
			const s = seconds < 10 ? `0${seconds}` : seconds;
			const v = `${mins}:${s}`;
			return v;
		} else {
			return '';
		}
	}
}

@Pipe({name: 'byteSize'})
export class ByteSizePipe implements PipeTransform {
	transform(input: any, type = 'best') {
		const byteDivisor = 1024;
		const bytes = parseInt(input, 10);
		if (!isNaN(bytes)) {
			const kb = input / byteDivisor;
			const mb = kb / byteDivisor;
			const gb = mb / byteDivisor;
			if (gb > 1 || type === 'GB') {
				return `${gb.toFixed(2)}GB`;
			} else if (mb > 1 || type === 'MB') {
				return `${mb.toFixed(2)}MB`;
			} else if (kb > 1 || type === 'KB') {
				return `${kb.toFixed(2)}KB`;
			} else {
				return `${bytes} bytes`;
			}
		} else {
			return '';
		}
	}
}


export const FORMATTING_PIPES: any = [
	JoinPipe,
	HideEmptyPipe,
	TruncatePipe,
	HumanTimePipe,
	ByteSizePipe
];
