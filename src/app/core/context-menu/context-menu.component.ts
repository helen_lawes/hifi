import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-context-menu',
	templateUrl: './context-menu.component.html',
	styleUrls: ['./context-menu.component.scss']
})
export class ContextMenuComponent implements OnInit {

	@Input('menuId') menuId;
	show = false;

	constructor() {}

	ngOnInit() {}

	open() {
		this.show = true;
	}

	close() {
		this.show = false;
	}

}
