declare var io;
import { Injectable, EventEmitter } from '@angular/core';


@Injectable()
export class SocketService {

	private _socket;
	private _connection: EventEmitter<any> = new EventEmitter();

	constructor() {
		if (io) {
			const port = window.location.port === '3010' ? '8091' : '8090';
			const path = `${window.location.protocol}//${window.location.hostname}:${port}`;
			this._socket = io.connect(path);
			this._socket.on('connect', () => {
				this._connection.emit({type: 'connected'});
			});
			this._socket.on('disconnect', () => {
				this._connection.emit({type: 'disconnected'});
			});
			this._socket.on('app-error', (message) => {
				if (typeof message === 'string') { message = {message}; }
				message.type = 'error';
				this._connection.emit(message);
			});
			this._socket.on('message', (message) => {
				if (typeof message === 'string') { message = {message}; }
				message.type = 'message';
				this._connection.emit(message);
			});
		}
	}

	subscribe(fn) {
		return this._connection.subscribe(fn);
	}

	send(name, message = null) {
		this._socket.emit(name, message);
	}

}
