import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { ContextMenuComponent } from 'core/context-menu/context-menu.component';
import { MusicService } from 'core/music.service';

@Component({
	selector: 'app-songs-list',
	templateUrl: './songs-list.component.html',
	styleUrls: ['./songs-list.component.scss']
})
export class SongsListComponent implements OnInit, OnDestroy {

	@Input('songs') songs;
	@Input('limit') limit;
	@Input('playAll') playAll = true;
	@Input('playlistFile') playlistFile = null;
	@Input('albumMode') albumMode = false;
	@ViewChildren(ContextMenuComponent) contextMenus: QueryList<ContextMenuComponent>;

	openedMenu = null;
	playing = false;
	currentSong = null;
	subscriptions = [];

	constructor(private _ms: MusicService) {
		this.subscriptions.push(this._ms.subscribe((event) => {
			this.processCurrentSong(event);
		}));
	}

	ngOnInit() {
		if (!this.limit) { this.limit = this.songs.length; }
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sub => {
			sub.unsubscribe();
		});
	}

	processCurrentSong(event) {
		if (event.type === 'current-song') {
			const info = event.message;
			if (info) {
				this.updateCurrentSong(info.id, info.status);
			}
		}
	}

	updateCurrentSong(songId, status) {
		if (!this.songs.length) { return; }
		let songPlaying = false;
		let currentSong = null;
		this.songs.forEach(song => {
			if (song.id === songId) {
				song.current = true;
				song.playing = status === 'playing';
				songPlaying = song.playing;
				currentSong = song;
			} else {
				song.current = false;
				song.playing = false;
			}
		});
		this.playing = songPlaying;
		this.currentSong = currentSong;
	}

	play(position, song) {
		if (!this.playAll) {
			this._ms.playOne(song);
		} else if (this.playlistFile) {
			this._ms.play(song.id, position, this.playlistFile);
		}	else {
			this._ms.play(song.id, position);
		}
	}

	pause() {
		this._ms.pause();
	}

	queueOne(song) {
		this._ms.queueOne(song);
	}

	queueNextSong(song) {
		this._ms.queueNextSong(song);
	}

	resetIcons(active) {
		this.songs.forEach((song) => {
			if (song.id !== active.id) {
				song.playing = false;
			}
		});
	}

	openMenu(id) {
		this.closeMenu();
		const menu = this.contextMenus.filter(menuItem => menuItem.menuId === id)[0];
		if (menu) {
			this.openedMenu = id;
			menu.open();
		}
	}

	closeMenu() {
		if (!this.openedMenu) { return; }
		const menu = this.contextMenus.filter(menuItem => menuItem.menuId === this.openedMenu)[0];
		if (menu) {
			menu.close();
			this.openedMenu = null;
		}
	}

}
