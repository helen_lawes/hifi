import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ContextMenuComponent } from '../context-menu/context-menu.component';

@Component({
	selector: 'app-context-menu-item',
	templateUrl: './context-menu-item.component.html',
	styleUrls: ['./context-menu-item.component.scss']
})
export class ContextMenuItemComponent implements OnInit {

	@Output('event') event: EventEmitter<any> = new EventEmitter(false);

	constructor(private menu: ContextMenuComponent) {}

	ngOnInit() {
	}

	clicked() {
		this.closeMenu();
		this.event.emit(true);
	}

	closeMenu() {
		this.menu.close();
	}

}
