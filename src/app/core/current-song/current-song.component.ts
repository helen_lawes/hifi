import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MusicService } from 'core/music.service';

@Component({
	selector: 'app-current-song',
	templateUrl: './current-song.component.html',
	styleUrls: ['./current-song.component.scss']
})
export class CurrentSongComponent implements OnInit {

	song = null;
	playing = false;
	expanded = false;
	ctime = 0;
	ttime = 0;
	translateX = null;

	constructor(private _ms: MusicService, private _router: Router) {
		this._ms.subscribe((event) => {
			if (event.type === 'current-song') {
				this.song = event.message;
				if (this.song) {
					this.ctime = this.convertToSeconds(this.song.ctime);
					this.ttime = this.convertToSeconds(this.song.ttime);
					if (this.song.status === 'playing') { this.playing = true; } else if (this.song.status === 'paused') { this.playing = false; }
				}
			}
		});
		this._router.events.subscribe((event) => {
			if ( event instanceof NavigationEnd) {
				if (event.url.match(/\(song:current\)/)) {
					this.expanded = true;
				} else {
					this.expanded = false;
				}
			}
		});
	}

	ngOnInit() {
	}

	togglePlay() {
		this._ms.play(this.song.id, this.song.position);
	}

	openPlayQueue() {
		this._router.navigateByUrl('/queue');
	}

	playNext() {
		this._ms.next();
	}

	playPrevious() {
		this._ms.previous();
	}

	toggleExpanded() {
		if (this.expanded) {
			this._router.navigate([{outlets: { song: null }}]);
		} else {
			this._router.navigate([{outlets: { song: ['current'] }}]);
		}
	}

	closeExpanded() {
		this._router.navigate([{outlets: { song: null }}]);
	}

	convertToSeconds(input) {
		if (!input) { return; }
		const match = input.match(/([0-9]{1,3}):([0-9]{2})/);
		if (match && match.length) {
			const s = parseInt(match[2], 10);
			const m = parseInt(match[1], 10);
			return (m * 60) + s;
		}
		return 0;
	}

	convertToMins(input) {
		if (!input) { return; }
		let seconds = parseInt(input, 10);
		if (!isNaN(seconds)) {

			let mins = Math.floor(seconds / 60);
			if (mins > 0) {
				seconds = (seconds - (mins * 60));
			}
			mins = mins || 0;
			const s = seconds < 10 ? `0${seconds}` : seconds;
			const v = `${mins}:${s}`;
			return v;
		} else {
			return '';
		}
	}

	onSlide(percentage) {
		this.ctime = this.ttime * (percentage / 100);
	}

	onSlideEnd(percentage) {
		this.ctime = this.ttime * (percentage / 100);
		this._ms.seek(this.convertToMins(this.ctime));
	}

}
