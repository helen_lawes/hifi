import { Component, Input, Output, EventEmitter, OnInit, ElementRef } from '@angular/core';


@Component({
	selector: 'app-inline-input',
	templateUrl: './inline-input.component.html'
})

export class InlineInputComponent implements OnInit {

	@Input('type') type = 'text';
	@Input('model') model;
	@Input('label') label;
	@Input('field') field;
	@Input('required') required = false;
	@Input('placeholder') placeholder = '';
	@Output('focus') focus: EventEmitter<any> = new EventEmitter(false);
	@Output('blur') blur: EventEmitter<any> = new EventEmitter(false);
	@Output('modelChange') modelChange: EventEmitter<any> = new EventEmitter(false);

	originalHeight = 0;

	constructor(private _el: ElementRef) {}

	ngOnInit() {
		if (this.type === 'textarea') {
			setTimeout(() => {
				this.originalHeight = this._getHeight();
				this._setHeight(this.originalHeight);
			}, 50);
		}
	}

	focused() {
		this.focus.emit(undefined);
	}
	blured() {
		this.blur.emit(undefined);
	}
	modelChanged() {
		this.modelChange.emit(this.model);
		if (this.type === 'textarea') this._setHeight(this._getHeight());
	}

	private _elementExists() {
		if (
			(typeof this._el === 'undefined' || !this._el) ||
			(typeof this._el.nativeElement === 'undefined') ||
			(typeof this._el.nativeElement.firstElementChild === 'undefined')
		) {
			return false;
		}
		return true;
	}
	private _getHeight(auto: boolean = true) {
		if (!this._elementExists()) return;
		if (auto) this._setHeight('auto');
		return this._el.nativeElement.firstElementChild.scrollHeight + 2;
	}
	private _setHeight(height) {
		if (!this._elementExists() || !height) return;
		if (this.model) {
			this._el.nativeElement.firstElementChild.style.height = height !== 'auto' ? height + 'px' : this.originalHeight + 'px';
		} else {
			this._el.nativeElement.firstElementChild.style.height = false;
		}
	}

}

@Component({
	selector: 'app-fancy-input',
	templateUrl: './fancy-input.component.html'
})

export class InputComponent implements OnInit {

	active: boolean;
	filled: boolean;
	@Input('type') type = 'text';
	@Input('model') model;
	@Input('label') label;
	@Input('field') field;
	@Input('required') required = false;
	@Input('placeholder') placeholder = '';
	@Output('modelChange') modelChange: EventEmitter<any> = new EventEmitter(false);

	ngOnInit() {
		this.filled = this.model != null && this.model.length > 0;
	}
	focused() {
		this.active = true;
	}
	blured() {
		this.active = false;
		this.filled = this.model != null && this.model.length > 0;
	}
	modelChanged($event) {
		this.modelChange.emit(this.model);
	}

}

@Component({
	selector: 'app-input-toggle',
	templateUrl: './input-toggle.component.html'
})

export class InputToggleComponent {

	@Input('model') model;
	@Output('modelChange') modelChange: EventEmitter<any> = new EventEmitter(false);

	toggle() {
		this.model = !this.model;
		this.modelChange.emit(this.model);
	}

}


export const FIELD_DIRECTIVES = [
	InlineInputComponent,
	InputComponent,
	InputToggleComponent
];
