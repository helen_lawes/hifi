import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-collections-list',
	templateUrl: './collections-list.component.html',
	styleUrls: ['./collections-list.component.scss']
})
export class CollectionsListComponent implements OnInit {

	@Input('collections') collections;
	@Input('route') route = '/playlist';

	constructor(private _router: Router) {}

	ngOnInit() {
	}

	viewPlaylist(collection) {
		const id = collection.id;
		this._router.navigate([this.route, id, collection.artist]);
	}


}
