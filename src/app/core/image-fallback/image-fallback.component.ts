import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-image-fallback',
	templateUrl: './image-fallback.component.html',
	styleUrls: ['./image-fallback.component.scss']
})
export class ImageFallbackComponent implements OnInit {

	@Input('src') src;
	@Input() fallbackSrc = 'assets/images/fallback.jpg';

	constructor() { }

	ngOnInit() {
	}

	fallback() {
		if (this.src === this.fallbackSrc) { return; }
		this.src = this.fallbackSrc;
	}

}
