import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ElementRef, Renderer, HostListener, ViewChild } from '@angular/core';

@Component({
	selector: 'app-slider',
	templateUrl: './slider.component.html',
	styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit, OnChanges {

	@Input('value') value;
	@Output('slidestart') slidestart: EventEmitter<any> = new EventEmitter(false);
	@Output('slide') slide: EventEmitter<any> = new EventEmitter(false);
	@Output('slideend') slideend: EventEmitter<any> = new EventEmitter(false);
	@ViewChild('handle') handle;
	@ViewChild('progress') progress;
	moving = false;
	original = null;
	sliderWidth = 0;
	percentage = 0;

	constructor(private el: ElementRef, private renderer: Renderer) { }

	ngOnInit() {
		this.sliderWidth = this.getSliderWidth();
	}

	ngOnChanges(record) {
		this.value = parseInt(this.value, 10);
		this.setPercentage(this.value);
	}

	@HostListener('window:resize')
	onResize() {
		this.sliderWidth = this.getSliderWidth();
	}

	@HostListener('mousedown', ['$event'])
	onMouseDown(e) {
		this.setPosition(e.clientX, e.clientY);
		this.pickUp();
	}

	@HostListener('document:mousemove', ['$event'])
	onMouseMove(e) {
		if (this.moving) {
			this.moveTo(e.clientX, e.clientY);
		}
	}

	@HostListener('document:mouseup')
	onMouseUp() {
		this.putBack();
	}

	@HostListener('touchstart', ['$event'])
	onTouchStart(e) {
		e.stopPropagation();
		this.setPosition(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
		this.pickUp();
	}

	@HostListener('document:touchmove', ['$event'])
	onTouchMove(e) {
		e.stopPropagation();
		if (this.moving) {
			this.moveTo(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
		}
	}

	@HostListener('document:touchend')
	onTouchEnd() {
		this.putBack();
	}

	private getSliderWidth() {
		let width = parseInt(this.el.nativeElement.style.width.replace('px', ''), 10);
		if (window) {
			width = parseInt(window.getComputedStyle(this.el.nativeElement, null).getPropertyValue('width'), 10);
		}
		return width;
	}

	private getHandlePosition() {
		let left = parseInt(this.handle.nativeElement.style.left.replace('px', ''), 10);
		let top = parseInt(this.handle.nativeElement.style.top.replace('px', ''), 10);
		if (window) {
			left = parseInt(window.getComputedStyle(this.handle.nativeElement, null).getPropertyValue('left'), 10);
			top = parseInt(window.getComputedStyle(this.handle.nativeElement, null).getPropertyValue('top'), 10);
		}
		return {left, top};
	}

	private setPosition(x, y) {
		const {left, top} = this.getHandlePosition();
		this.original = {
			x: x - left,
			y: y - top
		};
	}

	private moveTo(x, y) {
		if (this.original) {
			const newX = x - this.original.x;
			if (newX >= 0 && newX <= this.sliderWidth) {
				const percentage = (newX / this.sliderWidth) * 100;
				this.setPercentage(percentage);
				this.slide.emit(percentage);
			}
		}
	}

	private setPercentage(percentage) {
		this.percentage = percentage;
		this.renderer.setElementStyle(this.handle.nativeElement, 'left', `${this.percentage}%`);
		this.renderer.setElementStyle(this.progress.nativeElement, 'width', `${this.percentage}%`);
	}

	private pickUp() {
		this.moving = true;
		this.slidestart.emit();
	}

	private putBack() {
		if (this.moving) {
			this.moving = false;
			this.original = false;
			this.value = this.percentage;
			this.slideend.emit(this.percentage);
		}
	}

}
