import { Component, OnInit , ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationStart } from '@angular/router';
import { MenuService } from 'core/menu.service';

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

	search = null;
	sideMenuOpen = false;
	backButton = false;
	searchButton = true;
	searchField = true;
	saveButton = false;
	menuHeading = null;

	constructor(private _router: Router, private _location: Location, private _el: ElementRef, private _menu: MenuService) {
		this._router.events.subscribe((event) => {
			if (event instanceof NavigationStart ) {
				this.resetMenu();
			}
		});
		this._menu.subscribe((event) => {
			if (event.type === 'showBack') {
				this.backButton = true;
			} else if (event.type === 'formPage') {
				this.backButton = true;
				this.saveButton = true;
				this.searchField = false;
				this.searchButton = false;
				this.menuHeading = event.heading;
			}
		});
	}

	ngOnInit() {
	}

	resetMenu() {
		this.backButton = false;
		this.saveButton = false;
		this.menuHeading = null;
		this.searchField = true;
		this.searchButton = true;
	}

	submitSearch() {
		if (this.search && this.search !== '') {
			if (this._elementExists()) {
				const inputs = this._el.nativeElement.getElementsByTagName('input');
				if (inputs.length) {
					inputs[0].blur();
				}
			}
			this._router.navigateByUrl(`/search/${this.search}`);
		}
	}

	goBack() {
		this._location.back();
	}

	navigate(location) {
		this._router.navigateByUrl(location);
		this.closeSideMenu();
	}

	save() {
		this._menu.formSave();
	}

	toggleSideMenu() {
		this.sideMenuOpen = !this.sideMenuOpen;
	}

	closeSideMenu() {
		this.sideMenuOpen = false;
	}

	openSideMenu() {
		this.sideMenuOpen = true;
	}

	keyEvents($event) {
		if ($event.key === 'Enter') {
			this.submitSearch();
		}
	}

	private _elementExists() {
		if (
			(typeof this._el === 'undefined' || !this._el ) ||
			(typeof this._el.nativeElement === 'undefined') ||
			(typeof this._el.nativeElement.firstElementChild === 'undefined')
		) {
			return false;
		}
		return true;
	}

}
