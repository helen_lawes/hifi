import { Component, OnInit, AfterViewInit, OnChanges, Input, ViewChild } from '@angular/core';
import { animate, style, transition, keyframes, AnimationBuilder, AnimationPlayer } from '@angular/animations';

@Component({
	selector: 'app-text-overflow',
	templateUrl: './text-overflow.component.html',
	styleUrls: ['./text-overflow.component.scss']
})
export class TextOverflowComponent implements OnChanges, OnInit, AfterViewInit {

	@Input('text') text;
	@Input('animationDuration') animationDuration = 15;
	@Input('animationCondition') animationCondition = true;
	@ViewChild('outer') outer;
	@ViewChild('inner') inner;
	outerWidth = 0;
	innerWidth = 0;
	player = null;

	constructor(private _builder: AnimationBuilder) { }

	ngOnChanges() {
		this.checkOverflow();
	}

	ngOnInit() {
	}

	ngAfterViewInit() {
		this.checkOverflow();
	}

	checkOverflow() {
		this.outerWidth = this.outer.nativeElement.clientWidth;
		this.innerWidth = this.inner.nativeElement.clientWidth;
		if (!this.animationCondition) {
			this.stopScroll();
			return;
		}
		if (this.innerWidth > this.outerWidth) {
			this.scroll();
		} else {
			this.stopScroll();
		}
	}

	scroll() {
		if (!this.animationCondition) {
			this.stopScroll();
			return;
		}
		const offsetSwitchRight = this.innerWidth / (this.innerWidth + this.outerWidth);
		const duration = Math.round((this.animationDuration / 1000) * this.innerWidth);
		const factory = this._builder.build(
			animate(`${duration}s 2s linear`, keyframes([
				style({ transform: `translateX(0)`, opacity: 1, offset: 0 }),
				style({ transform: `translateX(-100%)`, opacity: 1, offset: (offsetSwitchRight - 0.02) }),
				style({ transform: `translateX(-100%)`, opacity: 0, offset: (offsetSwitchRight - 0.01) }),
				style({ transform: `translateX(${this.outerWidth}px)`, opacity: 0, offset: offsetSwitchRight }),
				style({ transform: `translateX(${this.outerWidth}px)`, opacity: 1, offset: (offsetSwitchRight + 0.01) }),
				style({ transform: `translateX(0)`, opacity: 1, offset: 1 })
			]))
		);
		this.player = factory.create(this.inner.nativeElement, {});
		this.player.play();
		this.player.onDone(() => {
			this.scroll();
		});
	}

	stopScroll() {
		if (this.player) {
			this.player.pause();
			this.player = null;
		}
	}


}
