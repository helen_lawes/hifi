import { Injectable, EventEmitter } from '@angular/core';
import { SocketService } from './socket.service';

@Injectable()
export class MusicService {

	private _ms: EventEmitter<any> = new EventEmitter();
	private _collections = null;
	private _recentSearch = null;
	private _recentSearchResults = null;
	private _playState = null;

	constructor(private _socket: SocketService) {
		console.log('music service');
		this._socket.subscribe(event => {
			if (event && event.type) {
				const method = `_${event.type}`;
				if (this[method]) { this[method](event); }
			}
		});
	}

	subscribe(fn) {
		return this._ms.subscribe(fn);
	}

	_connected() {
		console.log('connected');
		this._ms.emit({type: 'connected'});
	}

	_disconnected() {
		console.log('disconnected');
		this._ms.emit({type: 'disconnected'});
	}

	_error(e) {
		console.log('error', e.message);
		this._ms.emit({type: 'error', message: e.message});
	}

	_message(e) {
		console.log('message', e.message);
		e.type = e.name;
		if (e.name === 'search') {
			e.type = 'songs';
			this._recentSearchResults = e.message;
		} else if (e.name === 'collections') {
			this._collections = e.message;
		} else if (e.name === 'songQueue') {
			e.type = 'songs';
		} else if (e.name === 'current-song') {
			const message = e.message;
			if (message) {
				this._playState = message.status === 'playing';
			} else {
				this._playState = false;
			}
		}
		this._ms.emit(e);
	}

	getCollections() {
		if (this._collections) {
			this._message({name: 'collections', message: this._collections});
		} else {
			this._socket.send('collections');
		}
	}

	getStations() {
		this._socket.send('stations');
	}

	getPlaylist(id, route= 'playlist', type = null, refresh = false) {
		this._socket.send('playlist', {id, route, type, refresh});
	}

	getSongQueue() {
		this._socket.send('songQueue');
	}

	getSettings() {
		this._socket.send('getCache');
		this._socket.send('getSpeakers');
	}

	getSpeakers() {
		this._socket.send('getSpeakers');
	}

	toggleSpeaker(id, active) {
		this._socket.send('toggleSpeaker', {id, active});
	}

	updateSpeaker(speaker) {
		this._socket.send('updateSpeaker', speaker);
	}

	deleteSpeaker(id) {
		this._socket.send('deleteSpeaker', id);
	}

	search(term) {
		if (this._recentSearch && this._recentSearch === term) {
			setTimeout(() => {
				this._message({name: 'search', message: this._recentSearchResults});
			}, 50);
		} else {
			this._socket.send('search', term);
		}
		this._recentSearch = term;
	}

	play(id, position, file = 'queue.m3u') {
		this._socket.send('play', {id, position, file});
	}

	playOne(song) {
		this._socket.send('playOne', song);
	}

	queueOne(song) {
		this._socket.send('queueOne', song);
	}

	queueNextSong(song) {
		this._socket.send('queueNextSong', song);
	}

	pause() {
		this._socket.send('pause');
	}

	stop() {
		this._socket.send('stop');
	}

	current() {
		this._socket.send('current');
	}

	next() {
		this._socket.send('next');
	}

	previous() {
		this._socket.send('previous');
	}

	seek(time) {
		this._socket.send('seek', time);
	}
}
