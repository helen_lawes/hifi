import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-header-bar',
	templateUrl: './header-bar.component.html',
	styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit {

	@Input('mainHeading') mainHeading = false;
	@Input('secondaryHeading') secondaryHeading = false;
	@Input('actionButton') actionButton = false;
	@Input('actionIcon') actionIcon = false;
	@Input('image') image = false;
	@Input('short') short = false;
	@Output('event') event: EventEmitter<any> = new EventEmitter(false);

	constructor() { }

	ngOnInit() {
	}

	action() {
		this.event.emit({event: 'action'});
	}

}
