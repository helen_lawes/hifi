import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AlbumsListComponent } from './albums-list/albums-list.component';
import { ButtonComponent } from './button/button.component';
import { CollectionsListComponent } from './collections-list/collections-list.component';
import { ContextMenuComponent } from './context-menu/context-menu.component';
import { ContextMenuItemComponent } from './context-menu-item/context-menu-item.component';
import { CurrentSongComponent } from './current-song/current-song.component';
import { ErrorMessageComponent } from './error-message/error-message.component';
import { FIELD_DIRECTIVES } from './field/field.component';
import { FORMATTING_PIPES } from './formatting.pipe';
import { HeaderBarComponent } from './header-bar/header-bar.component';
import { ImageFallbackComponent } from './image-fallback/image-fallback.component';
import { MenuComponent } from './menu/menu.component';
import { SliderComponent } from './slider/slider.component';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { SongsListComponent } from './songs-list/songs-list.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { TextOverflowComponent } from './text-overflow/text-overflow.component';

import { MusicService } from './music.service';
import { MenuService } from './menu.service';
import { SocketService } from './socket.service';


import { CoreRoutingModule } from './core-routing.module';

const CORE_COMPONENTS = [
	AlbumsListComponent,
	ButtonComponent,
	CollectionsListComponent,
	ContextMenuComponent,
	ContextMenuItemComponent,
	CurrentSongComponent,
	ErrorMessageComponent,
	FORMATTING_PIPES,
	FIELD_DIRECTIVES,
	HeaderBarComponent,
	ImageFallbackComponent,
	MenuComponent,
	SliderComponent,
	SnackbarComponent,
	SongsListComponent,
	SpinnerComponent,
	TextOverflowComponent,
];

@NgModule({
	declarations: CORE_COMPONENTS,
	imports: [
		CommonModule,
		FormsModule,
		CoreRoutingModule
	],
	exports: CORE_COMPONENTS,
	providers: [MusicService, MenuService, SocketService]
})
export class CoreModule { }
