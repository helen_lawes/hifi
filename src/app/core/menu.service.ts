import { Injectable, EventEmitter } from '@angular/core';
import { SocketService } from './socket.service';

@Injectable()
export class MenuService {

	private _ms: EventEmitter<any> = new EventEmitter();

	subscribe(fn) {
		return this._ms.subscribe(fn);
	}

	showBack() {
		this._ms.emit({type: 'showBack'});
	}

	formPage(heading) {
		this._ms.emit({type: 'formPage', heading});
	}

	formSave() {
		this._ms.emit({type: 'formSave'});
	}

}
