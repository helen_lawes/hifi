import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-albums-list',
	templateUrl: './albums-list.component.html',
	styleUrls: ['./albums-list.component.scss']
})
export class AlbumsListComponent implements OnInit {

	@Input('albums') albums;
	@Input('limit') limit;
	@Output('event') event: EventEmitter<any> = new EventEmitter(false);

	constructor() {}

	ngOnInit() {
		if (!this.limit) {
			this.limit = this.albums.length;
		}
	}

	view(album) {
		this.event.emit({event: 'view', album});
	}

}
