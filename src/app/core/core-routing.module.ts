import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrentSongComponent } from './current-song/current-song.component';

const routes: Routes = [
	{ path: 'current', component: CurrentSongComponent, outlet: 'song' }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CoreRoutingModule { }
