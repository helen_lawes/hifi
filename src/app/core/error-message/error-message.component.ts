import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-error-message',
	templateUrl: './error-message.component.html',
	styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent implements OnInit {

	@Input('error') error = false;
	@Output('retry') retry: EventEmitter<any> = new EventEmitter(false);

	constructor() { }

	ngOnInit() {
	}

	tryAgain() {
		this.retry.emit();
	}

}
