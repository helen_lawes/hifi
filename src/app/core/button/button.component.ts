import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-button',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

	@Input('size') size;
	@Input('variant') variant;
	@Input('depth') depth;
	@Input('align') align;

	constructor() { }

	ngOnInit() {
	}

	getClasses() {
		return {
			[`button--${this.size}`]: this.size,
			[`button--${this.variant}`]: this.variant,
			[`button--${this.depth}`]: this.depth,
			[`pull-${this.align}`]: this.align,
		};
	}

}
