import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-snackbar',
	templateUrl: './snackbar.component.html',
	styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {

	shown = false;
	timeout: any = false;

	@Input('button') button = false;
	@Output() action: EventEmitter<any> = new EventEmitter(false);

	constructor() { }

	ngOnInit() {
	}

	show(timeoutMs: any = false) {
		this.shown = true;
		if (this.timeout || !timeoutMs) { return; }
		this.timeout = setTimeout(() => {
			this.hide();
		}, timeoutMs);
	}

	hide() {
		this.shown = false;
		if (this.timeout) {
			clearTimeout(this.timeout);
			this.timeout = false;
		}
	}

	emitAction() {
		this.hide();
		this.action.emit(undefined);
	}

}
