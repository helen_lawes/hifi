import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'core/core.module';
import { SearchComponent } from './search.component';

import { SearchRoutingModule } from './search-routing.module';

@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		SearchRoutingModule
	],
	declarations: [SearchComponent]
})
export class SearchModule { }
