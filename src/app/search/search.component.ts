import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MusicService } from 'core/music.service';
import { ListEvents } from 'shared/list-events';

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent extends ListEvents implements OnInit, OnDestroy {

	albums = [];
	stations = [];
	searchTerm = null;
	subscriptions = [];
	expand: any = false;
	albumLimit = 8;
	songLimit = 16;
	stationLimit = 8;

	constructor(protected _route: ActivatedRoute, protected _router: Router, protected _ms: MusicService) {
		super(_ms);
		this.playFrom = false;
		this.subscriptions.push(this._route.url.subscribe(url => {
			if (url.length > 2) {
				this.expand = url[2].path;
				if (this.expand === 'albums') {
					this.albumLimit = 0;
				}
				if (this.expand === 'songs') {
					this.songLimit = 0;
				}
				if (this.expand === 'stations') {
					this.stationLimit = 0;
				}
			}
		}));
		this.subscriptions.push(this._route.params.subscribe(params => {
			this.searchTerm = params['searchTerm'];
			this.getSearchResults();
		}));
		this.subscriptions.push(this._ms.subscribe((event) => {
			if (event.type === 'songs') {
				if (event.message) {
					this.songs = event.message.songs || [];
					this.albums = event.message.albums || [];
					this.stations = event.message.stations || [];
				}
				this.loading = false;
				this.error = false;
			} else if (event.type === 'search-error') {
				this.loading = false;
				this.error = true;
			}
		}));
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		this.subscriptions.forEach(sub => {
			sub.unsubscribe();
		});
	}

	albumEventEmitted(e) {
		const album = e.album;
		if (e.event === 'view') {
			this._router.navigate(['/album', album.id, this.escape(album.title)]);
		}
	}

	stationEventEmitted(e) {
		const { album: station } = e;
		if (e.event === 'view') {
			this._router.navigate(['/station', station.type, station.id, this.escape(station.title)]);
		}
	}

	escape(string) {
		return string.replace('(', '%28').replace(')', '%29');
	}

	view(path) {
		this._router.navigate(['/search', this.searchTerm, path]);
	}

	backToSearch() {
		this._router.navigate(['/search', this.searchTerm]);
	}

	getSearchResults() {
		this.loading = true;
		this.error = false;
		this.songs = [];
		this.albums = [];
		this._ms.search(this.searchTerm);
	}

}
