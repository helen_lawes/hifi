import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchComponent } from './search.component';

const routes: Routes = [
	{ path: 'search/:searchTerm', component: SearchComponent },
	{ path: 'search/:searchTerm/albums', component: SearchComponent },
	{ path: 'search/:searchTerm/songs', component: SearchComponent },
	{ path: 'search/:searchTerm/stations', component: SearchComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SearchRoutingModule { }
