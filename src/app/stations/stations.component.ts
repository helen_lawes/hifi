import { Component, OnInit } from '@angular/core';
import { MusicService } from 'core/music.service';

@Component({
	selector: 'app-stations',
	templateUrl: './stations.component.html',
	styleUrls: ['./stations.component.scss']
})
export class StationsComponent implements OnInit {

	songs = [];
	collections = [];
	search = '';
	loading = true;
	playing = false;
	error = false;

	constructor(private _ms: MusicService) {
		this._ms.subscribe((event) => {
			if (event.type === 'stations') {
				this.collections = event.message;
				this.loading = false;
				this.error = false;
			} else if (event.type === 'stations-error') {
				this.loading = false;
				this.error = true;
			}
		});
		this.getStations();
	}

	ngOnInit() {
	}

	getStations() {
		this.loading = true;
		this.error = false;
		this._ms.getStations();
	}

}
