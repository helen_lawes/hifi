import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'core/core.module';
import { StationsComponent } from './stations.component';

import { StationsRoutingModule } from './stations-routing.module';

@NgModule({
	imports: [
		CommonModule,
		CoreModule,
		StationsRoutingModule
	],
	declarations: [StationsComponent]
})
export class StationsModule { }
