const PROXY_CONFIG = [
    {
        context: [
            "/api",
            "/socket.io"
        ],
        target: "http://localhost:8090",
        secure: false
    }
];

module.exports = PROXY_CONFIG;
