let express = require('express'),
	mp = require('./helpers/music-player'),
	app = express(),
	server = require('http').Server(app),
	io = require('socket.io')(server),
	path = require('path'),
	publicFolder = path.resolve(__dirname + '/../dist'),
	indexFile = publicFolder + '/index.html';

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'X-Requested-With');
	next();
});

app.use(express.static(publicFolder));

app.use(['*'], function(req, res) {
	res.sendFile(indexFile);
});

server.listen(process.env.PORT || 8090);


let interval = null;
io.on('connection', function(socket) {
	getStatus = () => {
		mp.status().then((current) => {
				io.emit('message', {
					name: 'current-song',
					message: current
				});
				if (current.status && current.status === 'playing') {
					pollStatus();
				} else {
					stopPoll();
				}

			})
			.catch(e => {
				stopPoll();
				io.emit('message', {
					name: 'current-song',
					message: null
				});
			});
	};
	pollStatus = () => {
		if (interval) return;
		getStatus();
		interval = setInterval(getStatus, 1000);
	};
	stopPoll = () => {
		if (!interval) return;
		clearInterval(interval);
		interval = null;
	};
	pollStatus();
	socket.on('disconnect', () => {
		io.clients((err, clients) => {
			if (err || !clients.length) stopPoll();
		});
	});

	process.on('unhandledRejection', (reason, p) => {
		socket.emit('app-error', {
			name: 'promiseRejection',
			message: reason
		});
	});
	socket.emit('message', 'connected');

	require('./routes/search')(socket,pollStatus,getStatus);
	require('./routes/queue')(socket,pollStatus,getStatus);
	require('./routes/player')(socket,pollStatus,getStatus);
	require('./routes/playlist')(socket,pollStatus,getStatus);
	require('./routes/settings')(socket,pollStatus,getStatus);

});
