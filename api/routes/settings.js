let settings = require('../helpers/settings');

module.exports = (socket,getStatus,pollStatus)=> {
	socket.on('getCache', () => {
		settings.getCache().then((results) => {
				socket.emit('message', {
					name: 'cache',
					message: results
				});
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'cache',
					message: 'Could not get cache properties'
				});
			});
	});
	socket.on('getSpeakers', () => {
		settings.getSpeakers().then((results) => {
				socket.emit('message', {
					name: 'speakers',
					message: results
				});
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'speakers',
					message: 'Could not get speakers'
				});
			});
	});
	socket.on('toggleSpeaker',(value)=>{
		settings.toggleSpeaker(value.id,value.active).then((results)=>{
			socket.emit('message', {
				name: 'speakers',
				message: results
			});
		})
		.catch(e => {
			socket.emit('app-error', {
				name: 'speakers',
				message: 'Could not update speakers'
			});
		});
	});
	socket.on('updateSpeaker',(value)=>{
		settings.updateSpeaker(value).then((results)=>{
			socket.emit('message', {
				name: 'speakers',
				message: results
			});
		})
		.catch(e => {
			socket.emit('app-error', {
				name: 'speakers',
				message: 'Could not update speakers'
			});
		});
	});
	socket.on('deleteSpeaker',(value)=>{
		settings.deleteSpeaker(value).then((results)=>{
			socket.emit('message', {
				name: 'speakers',
				message: results
			});
		})
		.catch(e => {
			socket.emit('app-error', {
				name: 'speakers',
				message: 'Could not update speakers'
			});
		});
	});

};
