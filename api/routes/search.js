let search = require('../helpers/search');

module.exports = (socket,getStatus,pollStatus)=> {
	socket.on('search', (value) => {
		search.all(value).then((results) => {
				socket.emit('message', {
					name: 'search',
					message: results
				});
			})
			.catch(e => {
				socket.emit('message', {
					name: 'search-error',
					message: 'Could not get search results'
				});
			});
	});
};
