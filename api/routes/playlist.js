let collectionsList = require('../helpers/collections-list');

module.exports = (socket,getStatus,pollStatus)=> {
	socket.on('collections', () => {
		collectionsList.getPlaylists().then((collections) => {
				socket.emit('message', {
					name: 'collections',
					message: collections
				});
			})
			.catch(e => {
				socket.emit('message', {
					name: 'collections-error',
					message: 'Could not get collections'
				});
			});
	});
	socket.on('stations', () => {
		collectionsList.getStations().then((stations) => {
				socket.emit('message', {
					name: 'stations',
					message: stations
				});
			})
			.catch(e => {
				socket.emit('message', {
					name: 'stations-error',
					message: 'Could not get stations'
				});
			});
	});
	socket.on('playlist', (value) => {
		if (value.route === 'playlist') {
			collectionsList.getPlaylist(value.id).then((songs) => {
					socket.emit('message', {
						name: 'songs',
						message: songs,
						id: value.id
					});
				})
				.catch(e => {
					socket.emit('message', {
						name: 'playlist-error',
						message: 'Could not get playlist',
						id: value.id
					});
				});
		} else if (value.route === 'album') {
			collectionsList.getAlbum(value.id).then((songs) => {
					socket.emit('message', {
						name: 'songs',
						message: songs,
						id: value.id
					});
				})
				.catch(e => {
					socket.emit('message', {
						name: 'playlist-error',
						message: 'Could not get album playlist',
						id: value.id
					});
				});
		} else if (value.route === 'station') {
			if (value.type) {
				collectionsList.getStationByType(value.id, value.type).then((songs) => {
					socket.emit('message', {
						name: 'songs',
						message: songs,
						id: value.id
					});
				})
				.catch(e => {
					socket.emit('message', {
						name: 'playlist-error',
						message: 'Could not get album playlist',
						id: value.id
					});
				});
			} else {
				collectionsList.getStation(value.id).then((songs) => {
					socket.emit('message', {
						name: 'songs',
						message: songs,
						id: value.id
					});
				})
				.catch(e => {
					socket.emit('message', {
						name: 'playlist-error',
						message: 'Could not get album playlist',
						id: value.id
					});
				});
			}
		} else if(value.route === 'im-feeling-lucky') {
			collectionsList.getFeelingLucky(value.refresh).then((songs) => {
				socket.emit('message', {
					name: 'songs',
					message: songs,
					id: value.id
				});
			})
			.catch(e => {
				socket.emit('message', {
					name: 'playlist-error',
					message: 'Could not get album playlist',
					id: value.id
				});
			});
		}
	});
};
