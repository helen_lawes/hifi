let mp = require('../helpers/music-player');

module.exports = (socket,getStatus,pollStatus)=> {
	socket.on('play', (opts) => {
		mp.playSong(opts).then(() => {
				socket.emit('message', {
					name: 'play',
					message: opts
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'play',
					message: 'Could not play song'
				});
			});
	});
	socket.on('playOne', (song) => {
		let opts = { song, songOnly: true, id: song.id, position: 0 };
		mp.playSong(opts).then(() => {
				socket.emit('message', {
					name: 'play',
					message: song
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'playOne',
					message: 'Could not play song'
				});
			});
	});
	socket.on('pause', () => {
		mp.pause().then(() => {
				socket.emit('message', {
					name: 'pause'
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'pause',
					message: 'Could not pause song'
				});
			});
	});
	socket.on('next', () => {
		mp.next().then(() => {
				socket.emit('message', {
					name: 'next'
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'next',
					message: 'Could not switch to next song'
				});
			});
	});
	socket.on('previous', () => {
		mp.previous().then(() => {
				socket.emit('message', {
					name: 'previous'
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'previous',
					message: 'Could not switch to previous song'
				});
			});
	});
	socket.on('seek', (time) => {
		mp.seek(time).then(() => {
				socket.emit('message', {
					name: 'seek',
					message: time
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'seek',
					message: `Could not seek to ${time}`
				});
			});
	});
	socket.on('stop', () => {
		mp.stop().then(() => {
				socket.emit('message', {
					name: 'stop'
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'stop',
					message: 'Could not stop playing'
				});
			});
	});
	socket.on('current', () => {
		pollStatus();
	});
};
