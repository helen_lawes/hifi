let mp = require('../helpers/music-player');

module.exports = (socket,getStatus,pollStatus)=> {
	socket.on('songQueue', () => {
		pollStatus();
		mp.getSongQueue().then((songs) => {
				socket.emit('message', {
					name: 'songQueue',
					message: songs
				});
			})
			.catch(e => {
				socket.emit('message', {
					name: 'queue-error',
					message: 'Could not get song queue'
				});
			});
	});
	socket.on('queueOne', (value) => {
		mp.queueOne(value).then(() => {
				socket.emit('message', {
					name: 'queue',
					message: value
				});
				getStatus();
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'queueOne',
					message: 'Could not add song to queue'
				});
			});
	});
	socket.on('queueNextSong', (value) => {
		mp.queueNextSong(value).then(() => {
				socket.emit('message', {
					name: 'queueNextSong',
					message: value
				});
			})
			.catch(e => {
				socket.emit('app-error', {
					name: 'queueNextSong',
					message: 'Could not add song next in queue'
				});
			});
	});
};
