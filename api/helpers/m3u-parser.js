var COMMENT_RE, EXTENDED, comments, empty, extended, parse, simple;

EXTENDED = '#EXTM3U';

COMMENT_RE = /:(?:(-?[\d\.]+),(.+))\n(.+)/;

extended = function(line) {
	var match;
	match = line.match(COMMENT_RE);
	if (match && match.length === 4) {
		let image = match[2].split('[http');
		let parts = image[0].split(' - ');
		image = (image[1] || ']').replace(']', '');
		image = image !== '' ? 'http' + image : image;
		let file = match[3].trim();
		let idMatch = file.match(/id=(.+)/);
		return {
			length: match[1] || 0,
			artist: (parts[0] || '').replace(/&dash;/g, '-'),
			title: (parts[1] || '').replace(/&dash;/g, '-'),
			album: (parts[2] + (parts[3] ? ' - ' + parts[3] : '') || '').replace(/&dash;/g, '-'),
			file,
			image,
			id: idMatch[1]
		};
	}
};

simple = function(string) {
	return {
		file: string.trim()
	};
};

empty = function(line) {
	return !!line.trim().length;
};

comments = function(line) {
	return line[0] !== '#';
};

parse = function(playlist) {
	var firstNewline;
	playlist = playlist.replace(/\r/g, '');
	firstNewline = playlist.search('\n');
	if (playlist.substr(0, firstNewline) === EXTENDED) {
		return playlist.substr(firstNewline).split('\n#').filter(empty).map(extended);
	} else {
		return playlist.split('\n').filter(empty).filter(comments).map(simple);
	}
};

(typeof module !== "undefined" && module !== null ? module.exports : window).M3U = {
		name: 'm3u',
		parse: parse
	};
