const parser = require('./m3u-parser'),
	fs = require('./fs-promise'),
	path = require('path'),
	curl = require('./curl'),
	config = require('../config/gmusic.config'),
	mock = require('./mock').mock,
	m3u = parser.M3U;

let mockEnabled = mock.mockEnabled;

let query = {
	songs: search => {
		return new Promise((resolve, reject) => {
			let playListFile = path.normalize(
				`${config.playlistPath}songs.m3u`
			);

			let searchTerm = encodeURI(search.term);
			let searchType = search.type;
			let searchKey = searchType === 'artist' ? 'artist' : 'title';

			let secondTerm = search.secondTerm;
			let secondType = search.secondType;

			let params = '';
			if (secondTerm && secondTerm !== '') {
				secondTerm = encodeURI(secondTerm);
				if (secondType !== 'artist') {
					params = `type=${secondType}&title=${secondTerm}&artist=${searchTerm}`;
				} else {
					params = `type=${searchType}&title=${searchTerm}&artist=${secondTerm}`;
				}
			} else {
				params = `type=${searchType}&${searchKey}=${searchTerm}`;
			}

			curl(`${config.baseUrl}get_by_search?${params}`).then(
				fileData => {
					fs.writeFile(playListFile, fileData);
					let playlist = m3u.parse(fileData);
					resolve(playlist);
				},
				e => reject(e)
			);
		});
	},
	albums: search => {
		return new Promise((resolve, reject) => {
			let playListFile = path.normalize(
				`${config.playlistPath}albums.m3u`
			);
			let searchTerm = encodeURI(search.term);
			let searchType = search.type;
			if (searchType === 'artist') {
				curl(
					`${
						config.baseUrl
					}search_id?type=artist&artist=${searchTerm}`
				)
					.then(id => {
						if (id && id != '') {
							return curl(
								`${
									config.baseUrl
								}get_discography_artist?id=${id}`
							);
						}
						return Promise.resolve(null);
					})
					.then(fileData => {
						if (fileData) {
							fs.writeFile(playListFile, fileData);
							let playlist = m3u.parse(fileData);
							resolve(playlist);
						} else {
							resolve([]);
						}
					})
					.catch(e => reject(e));
			} else {
				resolve([]);
			}
		});
	},
	all: search => {
		return new Promise((resolve, reject) => {
			let playListFile = mockEnabled
				? mock.searchFile
				: path.normalize(`${config.playlistPath}search.json`);

			let searchTerm = encodeURI(search);

			params = `type=all&title=${searchTerm}`;

			fs
				.readFileAsJson(playListFile)
				.then(function(fileData) {
					if (
						(fileData && fileData.search.term === search) ||
						mockEnabled
					) {
						return fileData;
					} else {
						return curl(`${config.baseUrl}get_by_search?${params}`)
							.then(fileData => {
								fileData = JSON.parse(fileData);
								fileData.search = {
									term: search,
									time: new Date().getTime(),
								};
								fs.writeFile(
									playListFile,
									JSON.stringify(fileData, null, '\t')
								);
								return fileData;
							})
							.catch(e => reject(e));
					}
				})
				.then(fileData => {
					let {
						album_hits: albumHits,
						song_hits: songHits,
						station_hits: stationHits,
					} = fileData;
					let albums = [];
					let songs = [];
					let stations = [];

					songHits.forEach(song => {
						song = song.track;
						songs.push({
							album: song.album,
							artist: song.artist,
							file: `${config.baseUrl}get_song?id=${
								song.storeId
							}`,
							id: song.storeId,
							image: song.albumArtRef[0].url || '',
							length: song.durationMillis / 1000,
							title: song.title,
						});
					});

					albumHits.forEach(album => {
						album = album.album;
						albums.push({
							album: album.name,
							artist: album.artist,
							file: `${config.baseUrl}get_album?id=${
								album.albumId
							}`,
							id: album.albumId,
							image: album.albumArtRef || '',
							title: album.name,
						});
					});

					stationHits.forEach(station => {
						station = station.station;
						const image = ((station.compositeArtRefs && station.compositeArtRefs.find(img => img.aspectRatio === "1")) || (station.imageUrls && station.imageUrls.find(img => img.aspectRatio === "1")) || { url: ''}).url;
						const { seed } = station;
						const type = 'artistId' in seed ? 'artist' : 'trackId' in seed ? 'song' : 'albumId' in seed ? 'album' : null;
						if (type) {
							const id = seed[`${type === 'song' ? 'track' : type}Id`];
							stations.push({
								album: station.name,
								artist: '',
								file:
									`${
										config.baseUrl
									}get_new_station_by_id?id=${
										id
									}&type=${type}`,
								id,
								type,
								image,
								title: station.name,
							});
						}
					});

					resolve({
						songs,
						albums,
						stations,
					});
				})
				.catch(e => reject(e));
		});
	},
};

module.exports = query;
