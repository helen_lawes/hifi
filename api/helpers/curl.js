let exec = require('child_process').exec;

module.exports = (url) => {
	return new Promise((resolve,reject) => {
		console.log(url);
		exec(`curl "${url}" -s`, {maxBuffer: 600 * 1024}, (err,stdout,stderr)=>{
				if(err) reject(err);
				else resolve(stdout);
		});
	});
};
