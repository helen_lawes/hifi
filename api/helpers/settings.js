const fs = require('./fs-promise'),
	config = require('../config/gmusic.config'),
	path = require('path');

let settings = {

	getCache: () => {
		return new Promise((resolve,reject)=>{
			fs.readdir(config.playlistPath)
				.then(files=>{
					let statPromises = [];
					files.forEach((file)=>{
						let ext = path.extname(file);
						if(ext === '.m3u'){
							let fullPath = path.normalize(`${config.playlistPath}${file}`);
							statPromises.push(fs.stat(fullPath)
								.then((stats)=>{
									return stats.size;
								},reject));
						}
					},reject);

					Promise.all(statPromises)
						.then((data)=>{
							let size = data.reduce((a,b)=> a + b, 0);
							resolve({
								total: files.length,
								size,
								expiry: config.cacheExpiry
							});
						},reject);
				});
		});
	},

	getSpeakers: () => {
		return new Promise((resolve,reject)=>{
			resolve(config.speakers());
		});
	},

	toggleSpeaker: (speakerId,status) => {
		return new Promise((resolve,reject)=>{
			settings.getSpeakers()
				.then((speakers)=>{
					speakers.forEach((speaker)=>{
						if(speaker.id === speakerId){
							speaker.active = status;
						}
					});
					let data = JSON.stringify(speakers,null,'\t');
					fs.writeFile(config.speakerSettingsFilePath,data)
						.then(()=>{
							resolve(speakers);
						},e=> reject('failed writing file'));
				},e=> {
					reject('couldnt get speakers')
				});
		});
	},

	updateSpeaker: (updatedSpeaker) => {
		return new Promise((resolve,reject)=>{
			settings.getSpeakers()
				.then((speakers)=>{
					let found = false;
					speakers.forEach((speaker)=>{
						if(speaker.id === updatedSpeaker.id){
							Object.assign(speaker,updatedSpeaker);
							found = true;
						}
					});
					if(!found){
						speakers.push(updatedSpeaker);
					}
					let data = JSON.stringify(speakers,null,'\t');
					fs.writeFile(config.speakerSettingsFilePath,data)
						.then(()=>{
							resolve(speakers);
						},e=> reject('failed writing file'));
				},e=> {
					reject('couldnt get speakers')
				})
		});
	},

	deleteSpeaker: (id) => {
		return new Promise((resolve,reject)=>{
			settings.getSpeakers()
				.then((speakers)=>{
					let found = null;
					speakers.forEach((speaker,i)=>{
						if(speaker.id === id){
							found = i;
						}
					});
					if(found) {
						speakers.splice(found,1);
					}
					let data = JSON.stringify(speakers,null,'\t');
					fs.writeFile(config.speakerSettingsFilePath,data)
						.then(()=>{
							resolve(speakers);
						},e=> reject('failed writing file'));
				},e=> {
					reject('couldnt get speakers')
				})
		});
	}

};

module.exports = settings;
