const fs = require('./fs-promise'),
	path = require('path'),
	environment = require('../config/environment.json');

class Mock{
	constructor() {
		if (environment.mock) {
			fs
				.readFile(path.normalize(`${__dirname}/../mock/mpc-paused.txt`))
				.then(data => {
					this._pausedStatus = data.toString('utf8');
				});
			fs
				.readFile(
					path.normalize(`${__dirname}/../mock/mpc-playing.txt`)
				)
				.then(data => {
					this._playingStatus = data.toString('utf8');
				});
		}
		this.mockEnabled = environment.mock || false;
		this.status = 'paused';
		this.searchFile = path.normalize(`${__dirname}/../mock/search.json`);
	}

	run(prop) {
		return new Promise((resolve, reject) => {
			if (prop === 'pause') {
				this.status = 'paused';
			} else if (prop.match('play')) {
				this.status = 'playing';
			}
			if (this.status === 'paused') {
				resolve(this._pausedStatus);
			} else {
				resolve(this._playingStatus);
			}
		});
	}
};

module.exports = {
	mock: new Mock(),
};
