const parser = require('./m3u-parser'),
	fs = require('./fs-promise'),
	path = require('path'),
	curl = require('./curl'),
	config = require('../config/gmusic.config'),
	m3u = parser.M3U;

let collection = {
	_saveRequest: (url,file, refresh = false) => {
		return new Promise((resolve,reject) => {

			let playListFile = path.normalize(`${config.playlistPath}${file}`);

			fs.readFile(playListFile)
				.then(function(fileData){
					if (refresh) return collection._getRequest(url, playListFile);
					return fileData.toString();
				}, ()=> collection._getRequest(url, playListFile))
				.then((fileData)=>{
					let playlist = m3u.parse(fileData);
					resolve(playlist);
				},(e)=>reject(e));

		});
	},
	_getRequest: (url, playListFile) => {
		return curl(`${config.baseUrl}${url}`).then((fileData)=>{
			fs.writeFile(playListFile, fileData);
			return fileData;
		});
	},
	getPlaylists: () =>{
		return collection._saveRequest('get_all_playlists','collections-playlists.m3u');
	},
	getStations: () =>{
		return collection._saveRequest('get_all_stations','collections-stations.m3u');
	},
	getPlaylist: (id) =>{
		return collection._saveRequest(`get_playlist?id=${id}`,`playlist-${id}.m3u`);
	},
	getAlbum: (id) =>{
		return collection._saveRequest(`get_album?id=${id}`,`album-${id}.m3u`);
	},
	getStation: (id) =>{
		return collection._saveRequest(`get_station?id=${id}`,`station-${id}.m3u`);
	},
	getStationByType: (id, type) =>{
		return collection._saveRequest(`get_new_station_by_id?id=${
			id
		}&type=${type}`,`station-${type}-${id}.m3u`);
	},
	getFeelingLucky(refresh = false) {
		return collection._saveRequest(`get_ifl_station`,`ifl.m3u`, refresh);
	}
};
module.exports = collection;
