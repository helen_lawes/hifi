let exec = require('child_process').exec,
	fs = require('./fs-promise'),
	path = require('path'),
	config = require('../config/gmusic.config'),
	parser = require('./m3u-parser'),
	mock = require('./mock').mock,
	m3u = parser.M3U;

let mockEnabled = mock.mockEnabled;

let mp = {
	_cmd: prop => {
		return new Promise((resolve, reject) => {
			if (mockEnabled) {
				mock.run(prop).then(data => {
					resolve(data);
				});
			} else {
				exec(`mpc ${prop}`, (err, stdout) => {
					if (err) reject(err);
					else resolve(stdout);
				});
			}
		});
	},
	_readPlaylist: (file, returnAs = 'string') => {
		return fs
			.readFile(path.normalize(`${config.playlistPath}${file}`))
			.then(data => {
				if (returnAs === 'object') {
					data = data.toString('utf8');
					if (!data || data === '') return {};
					else return m3u.parse(data);
				} else {
					return data;
				}
			});
	},
	_formatString: (string, type = 'normal') => {
		if (type === 'm3u') {
			return string.replace(/-/g, '&dash;').trim();
		} else {
			return (string && string.replace(/&dash;/g, '-')) || '';
		}
	},
	_formatRecord: (song, start = `#EXTM3U\n`) => {
		let record = `${start}#EXTINF:${song.length},${mp._formatString(
			song.artist,
			'm3u'
		)} - ${mp._formatString(song.title, 'm3u')} - ${mp._formatString(
			song.album,
			'm3u'
		)} [${song.image.trim()}]\n${song.file}\n`;
		return record;
	},
	_formatRecords: (songs, start = `#EXTM3U\n`) => {
		let records = [start];
		songs.forEach((song, i) => {
			let record = mp._formatRecord(song, '');
			records.push(record);
		});
		return records.join('');
	},
	_readQueue: (returnAs = 'string') => {
		return mp._readPlaylist(config.queueFile, returnAs);
	},
	_writeQueue: data => {
		return fs.writeFile(config.queueFilePath, data);
	},
	_loadQueue: () => {
		return mp.load(config.queueFile);
	},
	_convertStatus: statusStr => {
		let regex = /(.+)\r?\n(\[(paused|playing)\]\s+#([^\/]+)\/([^\s]+)\s+([^\/]+)\/([^\s]+)\s+\(([0-9]{1,3})%\))\r?\n(.+)/;
		let matches = statusStr.match(regex);
		if (matches) {
			let [
				,
				/*wholeMatch*/ songDetails /*statusline*/,
				,
				status,
				position,
				total,
				ctime,
				ttime,
				percent,
			] = matches;
			let [artist, title, albumname] = songDetails.split(' - '),
				album = (albumname && albumname.split('[')[0]) || '';
			return {
				title: mp._formatString(title),
				artist: mp._formatString(artist),
				album: mp._formatString(album),
				status,
				position,
				total,
				ctime,
				ttime,
				percent,
			};
		} else {
			return null;
		}
	},
	playSong: ({ file, id, position, song, songOnly }) => {
		return mp.status().then(status => {
			position = parseInt(position);
			if (status) {
				status.position = parseInt(status.position);
				if (status.id === id) {
					if (
						status.status === 'playing' &&
						(status.position === position || position === 0)
					) {
						return mp.pause();
					} else {
						return mp.play();
					}
				} else {
					position = position || 1;
					if (songOnly) {
						return mp.playOne(song);
					} else {
						return mp.playFrom(file, position);
					}
				}
			} else {
				if (songOnly) {
					return mp.playOne(song);
				} else {
					return mp.play(file);
				}
			}
		});
	},
	play: (file = null) => {
		if (file) {
			return mp
				._readPlaylist(file)
				.then(data => {
					return mp._writeQueue(data);
				})
				.then(() => {
					return mp._loadQueue();
				})
				.then(() => {
					return mp._cmd('play');
				});
		}
		return mp._cmd('play');
	},
	playFrom: (file, position) => {
		return mp
			._readPlaylist(file)
			.then(data => {
				return mp._writeQueue(data);
			})
			.then(() => {
				return mp._loadQueue();
			})
			.then(() => {
				return mp._cmd(`play ${position}`);
			});
	},
	playOne: song => {
		return mp
			.clear()
			.then(() => {
				let record = mp._formatRecord(song);
				return mp._writeQueue(record);
			})
			.then(() => {
				return mp._loadQueue();
			})
			.then(() => {
				return mp.play();
			});
	},
	queueOne: song => {
		let record = mp._formatRecord(song, '');
		return mp
			._readQueue()
			.then(data => {
				return mp._writeQueue(`${data}\n${record}`);
			})
			.then(() => {
				return mp.add(song.file);
			})
			.then(() => {
				return mp.play();
			});
	},
	queueNextSong: song => {
		// if one song queue one then move to next position
		// if album then remove the other songs using `mpc crop`, add new songs and move current song to correct position
		return Promise.all([mp.getSongQueue(), mp.status()])
			.then(results => {
				let [songQueue, status] = results;
				let position = status.position;
				songQueue.splice(position, 0, song);
				let records = mp._formatRecords(songQueue);
				mp._writeQueue(records);
				return [position, songQueue.length];
			})
			.then(data => {
				return mp.add(song.file).then(() => {
					return data;
				});
			})
			.then(data => {
				let [position, length] = data;
				position = position / 1 + 1;
				return mp._cmd(`move ${length} ${position}`);
			});
	},
	add: file => {
		return mp._cmd(`add ${file}`);
	},
	pause: () => {
		return mp._cmd('pause');
	},
	next: () => {
		return mp._cmd('next');
	},
	previous: () => {
		return mp._cmd('prev');
	},
	seek: time => {
		return mp._cmd(`seek ${time}`);
	},
	stop: () => {
		return mp._cmd('stop');
	},
	clear: () => {
		return mp._cmd('clear');
	},
	load: file => {
		return mp.clear().then(() => {
			return mp._cmd(`load ${file}`);
		});
	},
	status: () => {
		return mp
			._cmd('status')
			.then(status => {
				return mp._convertStatus(status);
			})
			.then(status => {
				return mp._readQueue('object').then(data => {
					if (data && data.length && status) {
						let match;
						data.forEach(song => {
							if (
								song.title === status.title &&
								song.artist === status.artist &&
								song.album === status.album
							) {
								match = song;
							}
						});
						status = Object.assign(status, match);
					}
					return status;
				});
			});
	},
	getSongQueue: () => {
		return mp._readQueue('object');
	},
	clearSongQueue: () => {
		return mp._writeQueue('');
	},
};

module.exports = mp;
