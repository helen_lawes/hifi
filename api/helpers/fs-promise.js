const fs = require('fs');

module.exports = {
	readFile: (filePath, options = null) =>{
		return new Promise((resolve, reject) => {
			fs.readFile(filePath, options, (err, data) => {
				if (err) reject(err);
				else resolve(data);
			});
		});
	},
	readFileAsJson:(filePath, options = null) =>{
		return new Promise((resolve, reject) => {
			fs.readFile(filePath, options, (err, data) => {
				if (err) resolve(null);
				else {
					try{
						let json = JSON.parse(data);
						resolve(json);
					}
					catch(e){
						resolve(null);
					}
				}
			});
		});
	},
	writeFile: (filePath, data, options = null) =>{
		return new Promise((resolve, reject) => {
			fs.writeFile(filePath, data, options, (err) => {
				if (err) reject(err);
				else resolve();
			});
		});
	},
	stat: (filePath) =>{
		return new Promise((resolve,reject) => {
			fs.stat(filePath,(err, stats) => {
				if(err) reject(err);
				else resolve(stats);
			});
		});
	},
	readdir: (filePath, options = null) =>{
		return new Promise((resolve,reject) => {
			fs.readdir(filePath, options,(err, files) => {
				if(err) reject(err);
				else resolve(files);
			});
		});
	},
	unlink: (filePath) =>{
		return new Promise((resolve,reject) => {
			fs.unlink(filePath,(err) => {
				if(err) reject(err);
				else resolve();
			});
		});
	}
};
