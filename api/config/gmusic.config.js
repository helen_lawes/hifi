let path = require('path') ;

let gmConfig = {
	baseUrl:'http://localhost:9999/',
	playlistPath: path.normalize(`${__dirname}/../playlists/`),
	queueFile: 'queue.m3u',
	cacheExpiry: 7,
	speakerSettingsFile: 'speakers.json',
	speakers: () => {
		return require(`./${gmConfig.speakerSettingsFile}`);
	}
};
gmConfig.queueFilePath = path.normalize(`${gmConfig.playlistPath}/${gmConfig.queueFile}`);
gmConfig.speakerSettingsFilePath = path.normalize(`${__dirname}/${gmConfig.speakerSettingsFile}`);

module.exports = gmConfig;
