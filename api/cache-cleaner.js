const fs = require('./helpers/fs-promise'),
	config = require('./config/gmusic.config'),
	path = require('path');

fs.readdir(config.playlistPath)
	.then(files=>{
		files.forEach((file)=>{
			let ext = path.extname(file);
			if(ext === '.m3u'){
				let fullPath = path.normalize(`${config.playlistPath}${file}`);
				fs.stat(fullPath)
					.then((stats)=>{
						let last = stats.mtime;
						let date = new Date();
						let diff = date - last;
						var diffT = {
							s: diff / 1000
						};
						diffT.m = diffT.s / 60;
						diffT.h = diffT.m / 60;
						diffT.d = diffT.h / 60;
						if(diffT.d > config.cacheExpiry){
							console.log(`To delete: "${fullPath}"`);
							// fs.unlink(fullPath);
						}
					});
			}
		});

	});
